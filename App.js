import React from 'react';
import { NativeRouter, Route, Switch } from 'react-router-native';

import Home from './components/pages/Home';
import List from './components/pages/List';
import Details from './components/pages/Details';

export default function App() {
	return (
		<NativeRouter>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/list" component={List} />
				<Route path="/book/:id" component={Details} />
			</Switch>
		</NativeRouter>
	);
}
