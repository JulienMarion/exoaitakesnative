import React, { useRef } from 'react';
import { StyleSheet, View, Image, Dimensions, Animated, SafeAreaView } from 'react-native';
import img1 from '../img/img1.jpg';
import img2 from '../img/img2.jpg';
import img3 from '../img/img3.jpg';

const Carousel = () => {
	const { height, width } = Dimensions.get('window');
	const scrollX = useRef(new Animated.Value(0)).current;

	const img1Uri = Image.resolveAssetSource(img1).uri;
	const img2Uri = Image.resolveAssetSource(img2).uri;
	const img3Uri = Image.resolveAssetSource(img3).uri;

	const images = [
		{ uri: img1Uri, key: '1' },
		{ uri: img2Uri, key: '2' },
		{ uri: img3Uri, key: '3' }
	];

	const Indicators = () => {
		return (
			<View style={styles.indicators}>
				{images.map((item, i) => {
					const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
					const scale = scrollX.interpolate({
						inputRange,
						outputRange: [0.8, 1.2, 0.8],
						extrapolate: 'clamp'
					});
					const opacity = scrollX.interpolate({
						inputRange,
						outputRange: [0.4, 0.8, 0.4],
						extrapolate: 'clamp'
					});

					return (
						<Animated.View
							key={`indicator-${i}`}
							style={{
								backgroundColor: '#fff',
								borderRadius: 50,
								height: 12,
								margin: 6,
								opacity,
								transform: [{ scale }],
								width: 12
							}}></Animated.View>
					);
				})}
			</View>
		);
	};

	const styles = StyleSheet.create({
		images: {
			height: height / 2,
			width: width
		},
		slider: {
			alignItems: 'center',
			backgroundColor: '#fff',
			justifyContent: 'center'
		},
		indicators: {
			bottom: 20,
			flexDirection: 'row',
			position: 'absolute'
		}
	});

	return (
		<SafeAreaView style={styles.slider}>
			<Animated.FlatList
				data={images}
				horizontal
				keyExtractor={(item) => item.key}
				onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], { useNativeDriver: false })}
				scrollEventThrottle={32}
				pagingEnabled
				showsHorizontalScrollIndicator={false}
				renderItem={({ item }) => {
					return (
						<View>
							<Image source={{ uri: item.uri }} style={styles.images}></Image>
						</View>
					);
				}}
			/>
			<Indicators />
		</SafeAreaView>
	);
};
export default Carousel;
