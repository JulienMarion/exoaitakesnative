import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';

export default function Card(props) {
	const { height, width } = Dimensions.get('window');

	const styles = StyleSheet.create({
		layout: {
			borderRadius: 3,
			height: height / 3,
			width: width / 2.2
		}
	});

	return <View style={styles.layout}>{props.children}</View>;
}
