import React from 'react';
import { StyleSheet, Text } from 'react-native';
import Carousel from '../Carousel';
import Nav from '../Nav';

export default function Home() {
	const styles = StyleSheet.create({
		homeTitle: {
			fontSize: 40,
			marginTop: 10,
			textAlign: 'center'
		},
		homeContent: {
			fontSize: 14,
			flex: 1,
			padding: 25,
			textAlign: 'center'
		}
	});
	return (
		<>
			<Carousel />
			<Text style={styles.homeTitle}>Le plus beau titre</Text>
			<Text style={styles.homeContent}>
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, porro modi. Cum, earum voluptatum tempore, doloremque natus reprehenderit harum, vel iste hic voluptate suscipit vero
				porro molestias nihil quibusdam quasi?
			</Text>
			<Nav />
		</>
	);
}
