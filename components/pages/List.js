import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Text, Dimensions, StyleSheet, View, SafeAreaView } from 'react-native';
import Nav from '../Nav';
import BookList from '../books/BookList';

export default function List() {
	const [isLoading, setIsLoading] = useState(true);
	const [loadedBooks, setLoadedBooks] = useState([]);
	const { height, width } = Dimensions.get('window');

	useEffect(() => {
		fetch('https://api.nytimes.com/svc/books/v3//lists/2019-01-20/hardcover-fiction.json?api-key=RhXGv40S9EjGnbuVV4x8yx2W8EQNN74L')
			.then((response) => {
				return response.json();
			})
			.then((data) => {
				const bookList = data.results.books;
				const books = [];

				for (const key in bookList) {
					const book = {
						id: key,
						...bookList[key]
					};

					books.push(book);
				}
				setIsLoading(false);
				setLoadedBooks(books);
			})
			.catch((error) => {
				console.log(error);
			});
	}, [setIsLoading, setLoadedBooks]);

	const styles = StyleSheet.create({
		homeTitle: {
			fontSize: 40,
			marginTop: 20,
			textAlign: 'center'
		},
		loading: {
			height: height / 1.3,
			justifyContent: 'center',
			alignItems: 'center'
		},
		layout: {
			flexDirection: 'column',
			flex: 1
		}
	});

	return (
		<SafeAreaView style={styles.layout}>
			<Text style={styles.homeTitle}>Le plus beau titre</Text>
			{isLoading && (
				<View style={styles.loading}>
					<ActivityIndicator size="large" color="#0000ff" />
					<Text>Chargement..</Text>
				</View>
			)}
			{!isLoading && <BookList books={loadedBooks} />}
			<Nav />
		</SafeAreaView>
	);
}
