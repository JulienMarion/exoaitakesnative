import React from 'react';
import Card from '../ui/Card';
import { View, Text, StyleSheet, Image, Button, Dimensions } from 'react-native';
import { useHistory } from 'react-router-native';

export default function Details({
	location: {
		state: { image, title, author, description }
	}
}) {
	let history = useHistory();
	const onNavigateBack = () => {
		history.push('/list');
	};
	const { height, width } = Dimensions.get('window');
	const styles = StyleSheet.create({
		container: {
			display: 'flex',
			alignItems: 'center',
			height: height,
			width: width,
			marginTop: height / 6
		},
		image: {
			width: width / 2.5,
			height: height / 2.5,
			resizeMode: 'contain'
		},
		title: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		author: {
			textTransform: 'capitalize',
			fontStyle: 'italic'
		},
		desc: {
			padding: 5
		}
	});

	return (
		<View style={styles.container}>
			<Card>
				<Image source={{ uri: image }} style={styles.image} />
				<Text style={styles.title}>{title}</Text>
				<Text style={styles.author}>by: {author}</Text>
				<Text style={styles.desc}>{description}</Text>
				<Button title="back" onPress={onNavigateBack} />
			</Card>
		</View>
	);
}
