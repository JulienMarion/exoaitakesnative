import { FlatList, StyleSheet, Dimensions, SafeAreaView, View, TextInput } from 'react-native';
import React, { useState } from 'react';
import Book from './Book';

const BookList = ({ books }) => {
	const [search, setSearch] = useState('');
	const { height, width } = Dimensions.get('window');
	const styles = StyleSheet.create({
		list: {
			padding: 15,
			width: width
		},
		input: {
			borderWidth: 1,
			borderColor: '#777',
			padding: 8,
			margin: 8,
			width: width * 0.85
		}
	});

	const handleSearch = (list) => {
		return list.filter((book) => 
			book.author.toLowerCase().includes(search.toLowerCase()) || 
			book.title.toLowerCase().includes(search.toLowerCase())
		);
	};

	return (
		<FlatList
			ListHeaderComponent={
				<TextInput style={styles.input} onChangeText={(search)=>setSearch(search)} value={search} placeholder="Recherche..." />
			}
			style={styles.list}
			data={handleSearch(books)}
			numColumns={2}
			keyExtractor={(book) => book.id}
			renderItem={(book) => {
				return <Book {...book} />;
			}}
		></FlatList>

	);
};
export default BookList;
