import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Link } from 'react-router-native';
import Card from '../ui/Card';

export default function Book({ item }) {
	const styles = StyleSheet.create({
		images: {
			borderRadius: 3,
			height: 200,
			resizeMode: 'contain'
		}
	});
	return (
		<View>
			<Card>
				<Link
					to={{
						pathname: `/book/${item.id}`,
						state: {
							image: item.book_image,
							title: item.title,
							author: item.author,
							description: item.description
						}
					}}>
					<Image source={{ uri: item.book_image }} style={styles.images} />
				</Link>
			</Card>
		</View>
	);
}
